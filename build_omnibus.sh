#!/bin/bash -l

if [ "$#" -ne 3 ]; then
    echo "Usage: docker run ... <git_url> <tag|branch|commit> <omnibus_project_name>"
    exit -1
fi
git clone $1 /omnibus
cd /omnibus
git checkout $2
exec omnibus build -o=package_dir:/pkg $3
